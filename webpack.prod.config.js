const Dotenv = require('dotenv-webpack')
const TerserPlugin = require('terser-webpack-plugin')

module.exports = {
    mode: 'production',
    plugins: [
        new Dotenv({
            path: '.env.production',
        }),
    ],
    output: {
        chunkFilename: '[chunkhash].js',
    },
    optimization: {
        concatenateModules: false,
        minimize: true,
        minimizer: [
            new TerserPlugin({
                terserOptions: { keep_classnames: true },
            }),
        ],
    },
}
