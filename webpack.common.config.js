const HtmlWebPackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const { resolve } = require('path')

module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
            {
                test: /\.(css|scss)$/,
                use: [
                    { loader: 'style-loader' },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            localIdentName: '[local]__[hash:base64:5]',
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        },
                    },
                ],
                exclude: /node_modules/,
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
                include: /node_modules/,
            },
            {
                test: /.*?.svg$/,
                include: /src/,
                use: [
                    {
                        loader: '@svgr/webpack',
                        options: {
                            svgoConfig: {
                                plugins: {
                                    removeViewBox: false,
                                },
                            },
                        },
                    },
                ],
            },
            {
                test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                exclude: /src/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]',
                    },
                },
            },
            {
                test: /\.less$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader', // translates CSS into CommonJS
                    },
                ],
            },
        ],
    },
    resolve: {
        alias: {
            '#views': resolve(__dirname, 'src/views/'),
            '#remote': resolve(__dirname, 'src/remote/'),
            '#actions': resolve(__dirname, 'src/actions/'),
            '#components': resolve(__dirname, 'src/components/'),
            '#common': resolve(__dirname, 'src/common/'),
            '#logger': resolve(__dirname, 'src/logger/'),
            '#utils': resolve(__dirname, 'src/utils/'),
            '#public': resolve(__dirname, 'public'),
        },
    },
    entry: {
        app: './src/index.js',
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: './src/index.html',
            filename: './index.html',
            favicon: './favicon.ico',
        }),
        new CopyPlugin({
            patterns: [
                { from: 'public/', to: 'public' },
                { from: 'google645e1099d7f0210c.html' },
                { from: 'sitemap.xml' },
                { from: 'robots.txt' },
            ],
        }),
    ],
}
