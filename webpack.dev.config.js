const Dotenv = require('dotenv-webpack')

module.exports = {
    mode: 'development',
    devtool: 'eval-source-map',
    plugins: [
        new Dotenv({
            path: '.env.development',
        }),
    ],
    devServer: {
        port: '8081',
        host: '0.0.0.0',
        historyApiFallback: true,
        contentBase: './',
        hot: true,
    },
}
