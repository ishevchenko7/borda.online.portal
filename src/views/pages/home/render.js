import React from 'react'
import { Button } from 'antd'
import clsx from 'clsx'

import styles from './styles.scss'
import logoImage from '#public/images/logo.png'

export default function () {
    return (
        <div className={styles.home_page}>
            <div className={styles.fullscreen}>
                <div className={clsx(styles.split, styles.left)}>
                    <img className={styles.logo} src={logoImage} />

                    <div className={styles.quickstart}>
                        <div data-nosnippet>QUICKSTART</div>
                        <div>
                            <span data-nosnippet>
                                1. Register board with uniq name.
                            </span>
                            <span data-nosnippet>
                                2. Download server application.
                                <br />
                                On first run you need to create admin user.
                            </span>
                            <span data-nosnippet>
                                3. Open *.borda.online in browser.
                                <br />* - is board name.
                            </span>
                            <span data-nosnippet>
                                4. Login to your board like admin. Completed!
                                <br />
                                Now you can start sharing!
                            </span>
                        </div>
                    </div>

                    <div className={styles.menu}>
                        <Button
                            onClick={this.handleRegisterClick}
                            type='text'
                            size='large'
                        >
                            REGISTER
                        </Button>
                        <Button href='#download' type='text' size='large'>
                            DOWNLOAD
                        </Button>
                        <Button href='#faq' type='text' size='large'>
                            FAQ
                        </Button>
                        <Button href='#license' type='text' size='large'>
                            LICENSE
                        </Button>
                        <Button href='#contacts' type='text' size='large'>
                            CONTACTS
                        </Button>
                    </div>
                </div>
                <div
                    className={clsx(styles.split, styles.right, styles.slogan)}
                >
                    <p>CREATE HOME MEDIA ACCESS</p>
                </div>
            </div>

            <div id='download' className={styles.download_buttons}>
                <div data-nosnippet>DOWNLOAD</div>
                <div data-nosnippet name='win' onClick={this.handleDownloadClick}>
                    Windows (NSIS installer)
                </div>
                <div data-nosnippet name='linux' onClick={this.handleDownloadClick}>
                    Linux (portable AppImage for any Linux)
                </div>
            </div>

            <div id='faq' className={styles.content_unit}>
                <div data-nosnippet>FAQ</div>
                <div data-nosnippet>1. What is it?</div>
                <div>
                    Borda.online service allows you to create remote access to
                    your home media, and share it with friends. Inspired by an
                    idea that was in&nbsp;
                    <a
                        target='_blank'
                        href='https://wikipedia.org/wiki/Bulletin_board_system'
                    >
                        BBS
                    </a>
                    : when a PC became a station for sharing thematic content.
                </div>
                <div>2. Is that secure?</div>
                <div>
                    Yes. It use&nbsp;
                    <a
                        target='_blank'
                        href='https://wikipedia.org/wiki/Peer-to-peer'
                    >
                        P2P
                    </a>
                    &nbsp;connection with&nbsp;
                    <a
                        target='_blank'
                        href='https://wikipedia.org/wiki/Datagram_Transport_Layer_Security'
                    >
                        DTLS
                    </a>
                    &nbsp;or&nbsp;
                    <a
                        target='_blank'
                        href='https://wikipedia.org/wiki/Transport_Layer_Security'
                    >
                        TLS
                    </a>
                    &nbsp;protocols, i.e data is exchanging over direct
                    encrypted connections. All passwords stored as hashes.
                </div>
                <div>3. What does it consist of?</div>
                <div>
                    The service consists of two main parts: the desktop
                    application (server-side) running on a PC with media to
                    share, and client-side web application deployed on
                    *.borda.online
                </div>

                <div>4. What are the main technologies based on?</div>
                <div>
                    <a
                        target='_blank'
                        href='https://wikipedia.org/wiki/Peer-to-peer'
                    >
                        P2P
                    </a>
                    &nbsp;connection is on&nbsp;
                    <a target='_blank' href='https://wikipedia.org/wiki/WebRTC'>
                        WebRTC
                    </a>
                    , desktop application is on&nbsp;
                    <a
                        target='_blank'
                        href='https://wikipedia.org/wiki/Electron_(software_framework)'
                    >
                        Electron
                    </a>
                    , and web application (
                    <a
                        target='_blank'
                        href='https://wikipedia.org/wiki/Single-page_application'
                    >
                        SPA
                    </a>
                    ) on&nbsp;
                    <a
                        target='_blank'
                        href='https://wikipedia.org/wiki/React_(web_framework)'
                    >
                        React
                    </a>
                </div>
                <div>
                    7. What OS are supported by server-side desktop application?
                </div>
                <div>
                    Currently supported only Windows and Linux, for MacOS is on
                    roadmap.
                </div>
                <div>8. Is it open-source project?</div>
                <div>
                    Yes.
                    <br />
                    <a
                        target='_blank'
                        href='https://bitbucket.org/ishevchenko7/'
                    >
                        Link to source code
                    </a>
                    <br />
                    If you like to help project, please contact author:&nbsp;
                    <a href='mailto: sheva.igor@gmail.com'>
                        sheva.igor@gmail.com
                    </a>
                </div>
            </div>

            <div data-nosnippet id='license' className={styles.content_unit}>
                <div data-nosnippet>LICENSE</div>
                <span data-nosnippet>
                    This project is under MIT license
                    <br />
                    <br />
                    MIT License
                    <br />
                    <br />
                    Copyright (c) 2021 Igor Shevchenko
                    <br />
                    <br />
                    Permission is hereby granted, free of charge, to any person
                    obtaining a copy of this software and associated
                    documentation files (the "Software"), to deal in the
                    Software without restriction, including without limitation
                    the rights to use, copy, modify, merge, publish, distribute,
                    sublicense, and/or sell copies of the Software, and to
                    permit persons to whom the Software is furnished to do so,
                    subject to the following conditions:
                    <br />
                    <br />
                    The above copyright notice and this permission notice shall
                    be included in all copies or substantial portions of the
                    Software.
                    <br />
                    <br />
                    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
                    KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
                    WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
                    PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
                    OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
                    OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
                    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
                    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
                </span>
            </div>

            <div id='contacts' className={styles.content_unit}>
                <div>CONTACTS</div>
                <span>
                    <a href='mailto: admin@borda.online'>admin@borda.online</a>
                    <br />
                    <a href='mailto: sheva.igor@gmail.com'>
                        sheva.igor@gmail.com
                    </a>
                </span>
            </div>
        </div>
    )
}