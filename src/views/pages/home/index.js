import { navigate } from '@reach/router'
import PureModel from '#common/viewmo'

import render from './render'

const downloadUrls = {
    win: 'borda.online.app_setup.exe',
    linux: 'borda.online.app.zip',
}

class Home extends PureModel {
    constructor() {
        super({ render })
    }

    downloadFile(url) {
        const a = document.createElement('a')

        try {
            a.href = url
            a.download = url.split('/').pop()
            document.body.appendChild(a)
            a.click()
        } finally {
            document.body.removeChild(a)
        }
    }

    handleDownloadClick = (event) => {
        const os = event.currentTarget.getAttribute('name')
        this.downloadFile(`/download/${os}/${downloadUrls[os]}`)
    }

    handleRegisterClick = () => {
        navigate('/register')
    }
}

export default new Home()
