import React from 'react'
import ReCaptcha from 'react-google-recaptcha'
import { Row, Col, Card, Form, Input, Button } from 'antd'

import styles from './styles.scss'

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
}

const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
}

export default function () {
    const { codeConfirm } = this
    
    return this.settings ? (
        <div className={styles.container}>
            <Row
                type='flex'
                justify='center'
                align='middle'
                style={{ minHeight: '100vh' }}
            >
                <Col>
                    <Card title='Register'>
                        <Form
                            {...layout}
                            name='basic'
                            ref={this.setFormRef}
                            initialValues={{ remember: true }}
                            onFinish={this.onFinish}
                            onFinishFailed={this.onFinishFailed}
                        >
                            <Form.Item
                                name='email'
                                label='E-mail'
                                rules={[
                                    {
                                        type: 'email',
                                        message: 'Input is not valid E-mail',
                                    },
                                    {
                                        required: true,
                                        message: 'Input E-mail',
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label='Board name'
                                name='boardCode'
                                rules={[
                                    {
                                        required: true,
                                        pattern: new RegExp(/^[a-z0-9]{3,}$/g),
                                        message:
                                            'Unacceptable chars or very small name',
                                    },
                                ]}
                            >
                                <Input
                                    prefix='https://'
                                    suffix='.borda.online'
                                />
                            </Form.Item>

                            <Form.Item
                                name='password'
                                label='Password'
                                rules={[
                                    {
                                        required: true,
                                        pattern: new RegExp(/^.{6,}/g),
                                        message:
                                            'Specify password no less 6 symbols',
                                    },
                                ]}
                                hasFeedback
                            >
                                <Input.Password autoComplete='new-password' />
                            </Form.Item>

                            <Form.Item
                                name='confirm'
                                label='Confirm Password'
                                dependencies={['password']}
                                hasFeedback
                                rules={[
                                    {
                                        required: true,
                                        message: 'Confirm password',
                                    },
                                    ({ getFieldValue }) => ({
                                        validator(rule, value) {
                                            if (
                                                !value ||
                                                getFieldValue('password') ===
                                                    value
                                            ) {
                                                return Promise.resolve()
                                            }
                                            return Promise.reject(
                                                'Password and confirmation do not match'
                                            )
                                        },
                                    }),
                                ]}
                            >
                                <Input.Password autoComplete='new-password' />
                            </Form.Item>

                            <Form.Item {...tailLayout}>
                                <Button
                                    type='primary'
                                    htmlType='submit'
                                    loading={this.registrationPending}
                                >
                                    Register
                                </Button>
                            </Form.Item>

                            <ReCaptcha
                                ref={this.setReCaptchaRef}
                                size='invisible'
                                sitekey={this.settings.recaptchaSiteKey}
                            />
                            <codeConfirm.View />
                        </Form>
                    </Card>
                </Col>
            </Row>
        </div>
    ) : null
}
