import _ from 'lodash'

import PureModel from '#common/viewmo'
import render from './render'

class CodeConfirm extends PureModel {
    constructor(params) {
        super({ render })

        _.assign(this, { ...params })
    }

    show = () => {
        if (this.formRef) {
            this.formRef.resetFields()
        }

        this.update({
            visible: true,
        })
    }

    hide = () => {
        this.update({
            visible: false,
        })
    }

    setFormRef = (ref) => {
        this.formRef = ref
    }

    onOk = async () => {
        const { code } = await this.formRef.validateFields()
        this.parent.confirm(code)
    }
}

export default CodeConfirm
