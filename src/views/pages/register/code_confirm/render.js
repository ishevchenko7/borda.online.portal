import React from 'react'
import { Button, Modal, Form, Input } from 'antd'

import styles from './styles.scss'

export default function () {
    return (
        <Modal
            className={styles.modal}
            visible={this.visible}
            maskClosable={false}
            title='Confirmation by email code'
            footer={[
                <Button key='ok' type='primary' onClick={this.onOk}>
                    OK
                </Button>,
            ]}
        >
            <Form layout='vertical' name='confirmation' ref={this.setFormRef}>
                <Form.Item
                    name='code'
                    label='Please paste confirmation code from email'
                    rules={[
                        {
                            required: true,
                            message: 'Confirmation code is mandatory',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
            </Form>
        </Modal>
    )
}