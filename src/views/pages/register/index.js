import _ from 'lodash'
import { Modal } from 'antd'
import { navigate } from '@reach/router'

import { call } from '#remote'
import PureModel from '#common/viewmo'
import loadSettings from '#utils/load_settings'
import withSettings from '#common/settings-store/with_settings'

import CodeConfirm from './code_confirm'
import render from './render'

class RegisterPage extends PureModel {
    constructor() {
        super({ render })
    }

    async getToken() {
        return this.recaptchaRef.executeAsync()
    }

    onFinish = async (values) => {
        try {
            this.pending(true)

            const { success, timeout } = await call(
                {
                    type: 'send-confirmation-code',
                    args: {
                        ...values,
                        token: await this.getToken(),
                    },
                },
                this.settings.sendConfirmationCodeTimeout
            )

            if (success) {
                this.codeConfirm.show(timeout)
            }
        } finally {
            this.pending(false)
            this.recaptchaRef.reset()
        }
    }

    pending(value) {
        this.update({ registrationPending: value })
    }

    onFinishFailed = (errorInfo) => {}

    setReCaptchaRef = (ref) => {
        this.recaptchaRef = ref
    }

    setFormRef = (ref) => {
        this.formRef = ref
    }

    async onMount() {
        const codeConfirm = new CodeConfirm ({
            parent: this.codeConfirmHandlers,
        })

        await loadSettings()
        withSettings(this, 'registerPage')

        this.update({ codeConfirm })
    }

    codeConfirmHandlers = {
        confirm: async (confirmationCode) => {
            let success
            const values = await this.formRef.validateFields()

            this.codeConfirm.hide()
            this.pending(true)

            try {
                ;({ success } = await call({
                    type: 'register',
                    args: {
                        confirmationCode,
                        ...values,
                    },
                }))
            } finally {
                this.pending(false)
            }

            if (success) {
                this.formRef.resetFields()

                Modal.success({
                    title: 'Successful confirmation',
                    content: `Congratulation! Your board ${values.boardCode} with url https://${values.boardCode}.borda.online registration is done. 
                        For create access please download application`,
                    onOk: () => navigate('/'),
                })
            }
        },
    }
}

export default new RegisterPage()
