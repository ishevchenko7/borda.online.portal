import _ from 'lodash'
import logger from '#logger'

import signalHandler from './signal_handler'
import withSettings from '#common/settings-store/with_settings'
import createDataChannelWrapper from './data_channel_wrapper'

const RTCPeerConnection =
    window.mozRTCPeerConnection || window.webkitRTCPeerConnection

export default class RtcTransceiver extends EventTarget {
    constructor(params) {
        super()

        _.assign(this, params)
        withSettings(this)

        this.rtcConnection = new RTCPeerConnection({
            iceServers: this.settings.iceServers,
        })

        this.remoteStreams = {}
        this.remoteStreamResolvers = {}
        this.signalSenderPromises = []
        this.rtcConnection.addEventListener('track', this.onTrack)
        this.rtcConnection.addEventListener('datachannel', this.onDataChannel)
        this.rtcConnection.addEventListener(
            'negotiationneeded',
            this.onNegotiationNeeded
        )
        this.rtcConnection.addEventListener('icecandidate', this.onIceCandidate)
        this.rtcConnection.addEventListener(
            'iceconnectionstatechange',
            this.onIceConnectionStateChange
        )

        this.log = logger.child({
            class: this.constructor.name,
            token: this.user,
        })

        this.channelsReadyEventPromise = new Promise(
            (resolve) => (this.channelsReadyEventResolver = resolve)
        )

        if (this.negotiate) {
            this.createDataChannelWrappers()
        }
    }

    async readyState() {
        await this.channelsReadyEventPromise
        await Promise.all(this.signalSenderPromises)
    }

    async createDataChannelWrappers() {
        await this.createDataChannelWrapper('data')
        await this.createDataChannelWrapper('signaling')

        this.onChannelWrappersReady()
    }

    async createDataChannelWrapper(channel) {
        if (_.isString(channel)) {
            channel = this.rtcConnection.createDataChannel(channel, {
                ordered: false,
            })
            channel.binaryType = 'arraybuffer'
        }

        const common = {
            channel,
            log: this.log,
            settingsPath: `${this.settingsPath}.dataChannel`,
        }

        switch (channel.label) {
            case 'data':
                channel.binaryType = 'arraybuffer'
                this.dataChannelWrapper = await createDataChannelWrapper({
                    ...common,
                })
                break

            case 'signaling':
                this.signalingChannelWrapper = await createDataChannelWrapper({
                    onMessage: this.onSignalMessage,
                    ...common,
                })
                break
        }
    }

    async send(data) {
        await this.dataChannelWrapper.send(data)
    }

    async setMessageHandler(messageHandler) {
        await this.channelsReadyEventPromise
        this.dataChannelWrapper.onMessage = messageHandler
    }

    onChannelWrappersReady() {
        this.signalSender = this.signalingChannelWrapper.send
        this.channelsReadyEventResolver(this)
    }

    async getRemoteStream(id) {
        return (
            this.remoteStreams[id] ||
            (await new Promise((resolve, reject) => {
                this.remoteStreamResolvers[id] = resolve
            }))
        )
    }

    async addStream(constraints) {
        const stream = await navigator.mediaDevices.getUserMedia(constraints)
        _.forEach(stream.getTracks(), (track) =>
            this.rtcConnection.addTrack(track, stream)
        )

        return stream.id
    }

    removeTrack(id) {
        _.forEach(this.rtcConnection.getTransceivers(), ({ sender }) => {
            const { track } = sender

            if (track && track.id === id) {
                track.stop()
                this.rtcConnection.removeTrack(sender)
            }
        })
    }

    stopAllSenders() {
        _.forEach(this.rtcConnection.getTransceivers(), ({ sender }) => {
            const { track } = sender

            if (track) {
                track.stop()
                this.removeTrack(sender)
            }
        })
    }

    onSignalMessage = (data) => {
        signalHandler({ transceiver: this, signal: data })
    }

    onTrack = async (event) => {
        const [stream] = event.streams
        const { id } = stream

        this.remoteStreams[id] = stream

        if (this.remoteStreamResolvers[id]) {
            this.remoteStreamResolvers[id](stream)

            delete this.remoteStreams[id]
            delete this.remoteStreamResolvers[id]
        }
    }

    onDataChannel = async ({ channel }) => {
        await this.createDataChannelWrapper(channel)

        if (this.dataChannelWrapper && this.signalingChannelWrapper) {
            this.onChannelWrappersReady()
        }
    }

    onNegotiationNeeded = async () => {
        if (this.rtcConnection.signalingState === 'stable') {
            await this.rtcConnection.setLocalDescription(
                await this.rtcConnection.createOffer()
            )

            const offer = {
                type: 'offer',
                data: this.rtcConnection.localDescription,
            }
            this.signalSenderPromises.push(this.signalSender(offer))
            this.log.info({ offer })
        } else {
            this.log.info('Signaling state is not stable')
        }
    }

    onIceCandidate = async (event) => {
        if (event.candidate) {
            const candidate = {
                type: 'new_ice_candidate',
                data: event.candidate,
            }

            this.signalSenderPromises.push(this.signalSender(candidate))
            this.log.info({ candidate })
        } else {
            this.log.info('Last candidate event')
        }
    }

    onIceConnectionStateChange = (event) => {
        this.log.info(
            `State change to ${this.rtcConnection.iceConnectionState.toUpperCase()}`
        )

        switch (this.rtcConnection.iceConnectionState) {
            case 'closed':
            case 'failed':
            case 'disconnected':
                this.stopAllSenders()
                this.rtcConnection.close()
                this.dispatchEvent(new Event('close'))

                break
        }
    }
}
