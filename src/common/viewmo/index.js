import _ from 'lodash'
import React from 'react'

class PureModel {
    static modelsCounter = 0

    constructor({ type, ...other }) {
        _.assign(this, other)

        this.type = type || 'model'
        this.code = `${this.type}-${++PureModel.modelsCounter}`

        const model = this

        this.View = class extends React.PureComponent {
            componentDidMount() {
                if (model.component) {
                    console.error(
                        `Model ${model.code} already has rendered component`
                    )
                } else {
                    model.mounted = true
                    model.component = this
                    model.onMount()
                }
            }

            componentWillUnmount() {
                model.unmounted = true
                delete model.component
                model.onUnmount()
            }

            render() {
                return model.render.call(model, this.props)
            }
        }
    }

    update(fields) {
        let equal = false
        if (fields) {
            equal = true

            _.forOwn(fields, (value, key) => {
                equal = _.isEqual(this[key], value)

                if (!equal) {
                    return false
                }
            })
        }

        if (!equal) {
            Object.assign(this, fields)

            if (this.component) {
                this.component.forceUpdate()
            }
        }
    }

    onMount() {}
    
    onUnmount() {}
}

PureModel.stub = { View: () => null }

export default PureModel
