import { call } from '#remote'
import * as settingsStore from '#common/settings-store'

export default async function() {
    const { settings } = await call({
        type: 'get-settings',
    })
    settingsStore.set(settings)
}