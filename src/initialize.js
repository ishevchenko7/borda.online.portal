import * as settingsStore from '#common/settings-store'

export default async function () {
    settingsStore.set({
        actionsDispatcher: {
            remoteCallTimeout: 6000,
        },
    })
}
