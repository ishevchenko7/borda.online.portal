import { ActionsDispatcher } from '#common/signal-ready'

const actions = require.context('./', true, /^\.\/(?!index(\.js)?$)/)

class ServiceActionsDispatcher extends ActionsDispatcher {
    constructor(params) {
        super({ actions, ...params })
    }
}

export default ServiceActionsDispatcher
