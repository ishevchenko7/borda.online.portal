import _ from 'lodash'
import React from 'react'
import url from 'url'
import { Modal } from 'antd'

import logger from '#logger'
import { SocketTransceiver } from '#common/signal-ready'
import ServiceActionsDispatcher from '#actions/service'

let socket
let serviceActionsDispatcher

function connect() {
    return new Promise(function (resolve, reject) {
        if (socket) {
            switch (socket.readyState) {
                case WebSocket.OPEN:
                    resolve()
                    return

                case WebSocket.CONNECTING:
                    reject(new Error('Socket is not connected yet'))
                    return
            }
        }

        socket = new WebSocket(
            url.format({
                protocol: 'wss:',
                hostname: 'borda.online',
                pathname: '/ws/',
                query: { source: 'portal' },
            })
        )

        socket.onopen = async function () {
            logger.info('WebSocket open')

            serviceActionsDispatcher = new ServiceActionsDispatcher({
                transceiver: new SocketTransceiver({ socket }),
            })

            resolve()
        }

        socket.onclose = function (event) {
            logger.info('WebSocket close')

            switch (event.code) {
                case 4500:
                    reject(new Error('Internal error'))
                    break
            }

            socket = null
        }

        socket.onerror = function (event) {
            logger.info('WebSocket error')
            reject(new Error('Service connection error'))

            socket = null
        }
    })
}

export async function call(action, remoteCallTimeout) {
    try {
        await connect()
        return await serviceActionsDispatcher.remoteCall(
            action,
            remoteCallTimeout
        )
    } catch (error) {
        Modal.error({
            title: 'Error',
            content: (
                <div>
                    <p>{error.message}</p>
                </div>
            ),
        })

        throw error
    }
}
