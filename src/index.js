import React from 'react'
import { render, hydrate } from 'react-dom'
import { Router } from '@reach/router'
import 'antd/dist/antd.css'

import home from '#views/pages/home'
import register from '#views/pages/register'
import initialize from './initialize'

import './styles.scss'

function App() {
    return (
        <div>
            <Router>
                <home.View path='/' />
                <register.View path='register' />
            </Router>
        </div>
    )
}

async function start() {
    await initialize()

    const rootElement = document.getElementById('app')
    if (rootElement.hasChildNodes()) {
        hydrate(<App />, rootElement)
    } else {
        render(<App />, rootElement)
    }
}

start()
