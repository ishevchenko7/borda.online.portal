import pino from 'pino'

export default pino({
    browser: { asObject: true },
    timestamp: false,
    base: null,
})
